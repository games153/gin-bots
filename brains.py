#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Feb 18 13:23:33 2024

@author: zbrogie
"""

from gin import optimal_score, optimize_hand, find_sets, calc_score, check_no_dupes
from copy import copy

#class that makes all the decisions for the player, does all the thinking and remembering
#each brain gets assigned its player on construction of the player
#This class will be extended by other brains that do things differently
class Brain():
    def __init__(self):
        self.player = None
        self.discarded_cards = []
        self.opponent_cards = []
        
    #function that decides if we should draw from the discard pile or the deck at the beginning of our turn
    #returns "discard" for discard pile, "deck" for deck
    def make_draw_decision(self, shown_card):
        return("deck")
        
    #function that is called whenever the other player decides where to draw from, to keep your brain informed
    def process_other_draw_decision(self, shown_card):
        pass
        #put this in your memory bank somewhere
        
    #returns the card that you are going to discard
    def make_discard_decision(self):
        return(self.player.hand[3])#dumb decision
    
    #function that is called whenever the other player decides what to discard, to keep your brain informed
    def process_other_discard_decision(self, discarded):
        pass
        #put this in your memory bank somewhere

    #function gets called after you discard if you have 10 or fewer points in your hand.  return true if you want to knock, or false if you do not want to knock
    def make_knock_decision(self, shown_card):
        return(False)
    
#this simple brain always draws from the deck and wants to knock as soon as possible
class FastBrain(Brain):
    #knock as soon as you can
    def make_knock_decision(self, shown_card):
        return(True)
    
    #discard the highest card you have that is not in a set for the optimal score
    def make_discard_decision(self, hand = None):
        if hand is None:
            hand = self.player.hand
        sets = optimal_score(hand)[0]
        set_cards = [card for sett in sets for card in sett]
        non_set_cards = [card for card in hand if card not in set_cards]
        if len(non_set_cards) > 0:
            scores = [c.points for c in non_set_cards]
            max_index = scores.index(max(scores))
            return(non_set_cards[max_index])
        else:
            scores = [c.points for c in self.player.hand]
            max_index = scores.index(max(scores))
            return(hand[max_index])
        
#simple extension of fastbrain that may take the shown card if it is obviously beneficial
class FastBrain2(FastBrain):
    def make_draw_decision(self, shown_card):
        test_hand = self.player.hand + [shown_card]
        test_discard_card = self.make_discard_decision(hand = test_hand)
        test_hand.remove(test_discard_card)
        (_, test_score) =    optimal_score(test_hand)
        (_, current_score) = optimal_score(self.player.hand)
        if test_score < current_score:
            return("discard")
        else:
            return("deck")
    
    
    
    
#this simple brain wants to get a gin, not knock      
class SlowBrain(Brain):
    #never knock
    def make_knock_decision(self, shown_card):
        return(False)
    
    def make_draw_decision(self, shown_card):
        pass
    
    def rate_three(self, sett, four, hand):#rating based on "we are waiting for x number of cards out of y cards that could be right
        rating = 0
        return(rating)
    def rate_four(self):
        pass
    
    #returns the a list of lists.  the lists that it returns are the sets you should analyze as sets, and you should analyze other cards as non-sets
    def choose_best_sets(self, hand):
        options = []
        sets = find_sets(hand)
        threes = [sett for sett in sets if len(sett) == 3]
        fours = [sett for sett in sets if len(sett) == 4]
        for sett in fours:
            # get list of threes that can coexist with it
            compatible_threes = [three for three in threes if check_no_dupes([sett, three])]
            compatible_threes_ratings = [self.rate_three(x, sett, hand) for x in compatible_threes]
            best_rating = max(compatible_threes_ratings)
            best_three_index = compatible_threes_ratings.index(best_rating)
            best_three = compatible_threes(best_three_index)
            options.append((best_rating, [sett, best_three]))
        if len(options) > 0:
            option_ratings = [x[0] for x in options]
            best_option_index = option_ratings.index(max(option_ratings))
            return(options[best_option_index][1])
        elif len(fours) >0:
            #in this case we have a four and no threes
            options.append((self.rate_four()))
        #now we go into the case where there 
            
            
            
            
            
        
    def make_knock_decision(self, shown_card):
        return(False)



#this function calculates the optimal score in your hand if you were to discard each card from it, and returns the index of the card whose discard is optimal
def simple_optimize_discard(hand):
    scores = []
    test_hand = copy(hand)
    for current_card in hand:
        test_hand.remove(current_card)
        (_,score) = optimal_score(test_hand)
        scores.append(score)
        test_hand.append(current_card)
    best_index = scores.index(min(scores))
    return(best_index)









