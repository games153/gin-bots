#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Feb 18 13:31:40 2024

@author: zbrogie
"""

import time
from gin import Player
from gin import play_match
from brains import FastBrain3, FastBrain2
from reporters import Results_Reporter

    
def test_matches(n):
    player_0 = Player("zero-guy", FastBrain2())
    player_1 = Player("one-man", FastBrain2())
    score = [0,0]
    for i in range(n):
        winner = play_match(player_0, player_1, Results_Reporter())
        score[winner] += 1
        #print(f"Match won by player {winner}!")
    print(score)

def time_matches(n):
    begin = time.time()
    test_matches(n)
    end = time.time()
    print(f"Completed {n} matches in {int(end-begin)} seconds")

time_matches(300)