import random
import itertools
import time

class Card():
    def __init__(self, suit, number):
        self.suit = suit
        self.number = number
        self.points = min(10, number)

    def __str__(self):
        names = {1:"A",
                 2:"2",
                 3:"3",
                 4:"4",
                 5:"5",
                 6:"6",
                 7:"7",
                 8:"8",
                 9:"9",
                 10:"10",
                 11:"J",
                 12:"Q",
                 13:"K"}
        suits = {0:"S",
                 1:"C",
                 2:"H",
                 3:"D"}

        return(names[self.number]+suits[self.suit])# add suit
       
class Player():
    def __init__(self, name, brain):
        self.hand = []
        self.name = name
        self.score = 0
        self.brain = brain
        brain.player = self
    def game_over(self):
        self.hand = []
    

class Game():
    def __init__(self, player_0, player_1, first_turn, reporter):
        self.players = [player_0, player_1]
        self.deck = build_deck()
        random.shuffle(self.deck)
        self.discard_pile = []
        self.whos_turn = first_turn
        self.game_over = False
        self.reporter = reporter
        self.winner= None # fill this in with 0 or 1 when the game ends
        self.turns = 0
        self.turn_limit = 100
        
    #returns the player whos turn it is not
    def _get_other_turn(self):
        if self.whos_turn == 0:
            return(1)
        else:
            return(0)
    not_turn = property(
        fget=_get_other_turn,
        fset=None,
        fdel=None,
        doc="The player who is watching (not playing) during this turn"
    )
    
    #this function flip flops whos turn it is
    def change_whos_turn(self):
        self.whos_turn = self.not_turn
    
    def play(self):
        self.setup_game()
        while not self.game_over:
            self.play_turn()
            if self.turns > self.turn_limit:
                self.game_over = True # but don't change the score
                self.winner = self.whos_turn
                print("Game went on for too long")
        self.reporter.report_game_over(self.players[0].score, self.players[1].score)#print out the score etc, save results so we can play another game, etc
        [p.game_over() for p in self.players]
        return(self.winner)

    def setup_game(self):
        #deal cards to each player
        for i in range(10):
            for player in self.players:
                player.hand.append(self.deck.pop())
        #start discard pile
        self.discard_pile.append(self.deck.pop())
        
    def play_turn(self):
        self.turns += 1
        draw_decision = self.players[self.whos_turn].brain.make_draw_decision(self.discard_pile[-1])
        self.reporter.report_draw(self.players[self.whos_turn], draw_decision, self.discard_pile[-1])
        self.players[self.not_turn].brain.process_other_draw_decision(self.discard_pile[-1])
        self.process_draw(draw_decision)
        
        
        to_discard = self.players[self.whos_turn].brain.make_discard_decision()
        self.players[self.not_turn].brain.process_other_discard_decision(to_discard)
        self.process_discard(to_discard)
        self.reporter.report_discard(self.players[self.whos_turn], to_discard)

        (tmp, score) = optimal_score(self.players[self.whos_turn].hand)

        #check for a gin
        if score == 0:
            self.process_gin()
        #check for knock
        elif score <= 10:
            knock_decision = self.players[self.whos_turn].brain.make_knock_decision(self.discard_pile[-1])
            if knock_decision:
                self.process_knock() 
        #the turn is now over
        self.change_whos_turn()
        return()
        
        
    
    def process_draw(self, draw_decision):
        if draw_decision == "discard":
            chosen_deck = self.discard_pile
        elif draw_decision == "deck":
            #if the deck is empty, then take the discard pile, shuffle it back into the deck, and continue.
            if len(self.deck) == 0:
                self.deck = self.discard_pile[:-1]
                self.discard = [self.discard_pile[-1]]
                random.shuffle(self.deck)
            chosen_deck = self.deck
        else:
            print(f"illegal draw decision: {draw_decision}")
            
           
        
            
        self.players[self.whos_turn].hand.append(chosen_deck.pop())
  
    def process_discard(self, to_discard):
        self.discard_pile.append(to_discard)
        self.players[self.whos_turn].hand.remove(to_discard)

    def process_knock(self):
        #this is called when the  player whose turn it is knocks
        (winning_sets, winning_score) = optimal_score(self.players[self.whos_turn].hand)
        loser_hand = self.players[self.not_turn].hand
        loser_sets = optimal_score(loser_hand)[0]
        losing_score = calc_score(loser_hand, loser_sets, enemy_sets = winning_sets)
        
        if winning_score < losing_score:
            score_change = losing_score - winning_score
            self.players[self.whos_turn].score += score_change
            self.winner = self.whos_turn
        else:
            score_change = 10 + winning_score - losing_score
            self.players[self.not_turn].score += score_change
        self.reporter.report_knock(self.whos_turn, winning_score, losing_score)
        self.game_over = True
        self.winner = self.not_turn

    def process_gin(self):
        #this is called when the player whos turn it is gets a gin
        winning_sets = optimal_score(self.players[self.whos_turn].hand)[0]
        loser_hand = self.players[self.not_turn].hand
        loser_sets = optimal_score(loser_hand)[0]
        losing_score = calc_score(loser_hand, loser_sets, enemy_sets = winning_sets)
        self.players[self.whos_turn].score += 25 + losing_score
        self.reporter.report_gin(self.whos_turn,losing_score)
        self.game_over = True
        self.winner = self.whos_turn
        
        
        
        


#function that returns a deck full of cards, all 52 cards in order, as a list of card objects
def build_deck():
    deck = []
    suits = [0,1,2,3]
    numbers = range(1,14)
    for suit in suits:
        for number in numbers:
            deck.append(Card(suit, number))
    return(deck)
    
#calculates the score of the deadwood in the hand in the most optimal arrangement, and also the sets used for the most optimal arrangement
#returns a tuple (best_sets, score) best_sets is a list of lists of cards that optimize the score, score is the sum of the points of each deadwood card
def optimal_score(hand):
    sets = find_sets(hand) #sets is a list of list, each member list is comprised of either 3 or 4 cards
    (best_sets, score) = optimize_hand(hand, sets) #best_sets is sets with some of the element lists removed to minimize the deadwood score under the constraint that no sets intersect
    return(best_sets, score)

#calculate the sum of the points in your hand that are not in a set
#enemy_sets is the list of sets the enemy used, so you can get rid of some of your deadwood.  this parameter is optional, and if you can't combine your deadwood withyour opponent then dont provide it or provide it empty
def calc_score(hand, sets, enemy_sets = []):
    set_cards = [card for sett in sets for card in sett]
    deadwood = [card for card in hand if card not in set_cards]
    
    dead_sets = []# each element is a list, containing sets made out of deadwood combined with enemy sets
    
    #look at each enemy set to see what deadwood we can add to it
    for current_set in enemy_sets:
        set_numbers = [card.number for card in current_set]
        set_numbers.sort()
        if set_numbers[0]==set_numbers[1]: #if it is a set of same number
            for card in deadwood:
                if card.number == set_numbers[0]:
                    dead_sets += [[card]]

        #look at the runs
        else: #if it is not a set of the same kind it must be a run
            set_suit = current_set[0].suit
            #look to connect one or two cards at the end of this run
            for card in deadwood:
                if card.suit == set_suit and card.number + 1 == set_numbers[0]:
                    dead_sets += [[card]]
                    #check if the next card in the set is here too
                    for next_card in deadwood:
                        if next_card.suit == set_suit and next_card.number + 2 == set_numbers[0]:
                            dead_sets += [[card, next_card]]
                elif card.suit == set_suit and card.number - 1 == set_numbers[-1]:
                    dead_sets += [[card]]
                    #check if the next card in the set is here too
                    for next_card in deadwood:
                        if next_card.suit == set_suit and next_card.number - 2 == set_numbers[-1]:
                            dead_sets += [[card, next_card]]
    
    #now we have all the dead sets.  time to see which grouping gives us the best score.  
    if len(dead_sets) == 0:
        remaining_score = 0
        for card in deadwood:
            remaining_score += card.points
    else:
        (best_dead_sets, remaining_score) = optimize_hand(deadwood, dead_sets)
    
    return(remaining_score)

#finds all valid gin sets in the hand, some of which may be overlapping
#input is a ten card hand
# returns sets is a list of lists, each member list is comprised of either 3 or 4 cards
def find_sets(hand):
    sets = []
    #first find 3 or 4 of a kind
    for i in range(1,14):
        this_group = [card for card in hand if card.number==i]
        if len(this_group)==3:
            sets.append(this_group)
        if len(this_group)==4:
            sets.append(this_group)
            for n in range(4):
                sets.append(this_group[:n] + this_group[n + 1:])
    #print([str(card.number)+"-"+str(card.suit)+'...' for sett in sets for card in sett])
    #now get the runs
    for start in range(1,12):
        for suit in range(4):
            for run_length in [3,4]:
                allowed_values = range(start, start+run_length)
                this_group = [card for card in hand if card.suit==suit and card.number in allowed_values]#all the cards in the desired run
                if len(this_group) == run_length:
                    sets.append(this_group)
    return(sets)

#this function finds the optimal group of sets to select to give you the best score
#(best_sets, score) = optimize_hand(hand, sets) #best_sets is sets with some of the element lists removed to minimize the deadwood score under the constraint that no sets intersect
#input is the the hand and the list of sets found by find_sets, and enemy_sets is the list of sets the enemy used, so you can get rid of some of your deadwood.  this parameter is optional, and if you can't combine your deadwood withyour opponent then dont provide it or provide it empty
#output is a tuple,(best_sets, score) best_sets is a list of sets that optimize the score, score is the sum of the points of each deadwood card
def optimize_hand(hand, sets, enemy_sets = []):
    #fill sets_choices with all the subsets of sets
    sets_choices = [] 
    # =============================================================================
    #     print("============")
    #     for card in hand:
    #         print(card)
    #     for sett in sets:
    #         print("------")
    #         for card in sett:
    #             print(card)
    # =============================================================================
    for n in range(len(sets)+1):
        sets_choices += list(itertools.combinations(sets, n))
    #remove the sets that use the same card multiple times
    valid_set_choices = [set_choice for set_choice in sets_choices if check_no_dupes(set_choice)]
    #calculate scores and pick lowest one
    scores = [calc_score(hand, set_choice, enemy_sets = enemy_sets) for set_choice in valid_set_choices]
    optimal_index = scores.index(min(scores))

    return(valid_set_choices[optimal_index], scores[optimal_index])

#checks if set choices has any cards more than once.   if so it return false.  if no dupes, it returns true
#set_choices is a list of lists.   each individual list contains cards.
def check_no_dupes(set_choices):
    all_cards = [card for sett in set_choices for card in sett]
    if len(all_cards) == len(set(all_cards)):
        return(True)
    else:
        return(False)



def play_match(player_0, player_1, reporter):
    player_1.score = 0
    player_0.score = 0
    winning_score = 100
    starts_next_match = 0 
    while player_0.score < winning_score and player_1.score < winning_score:
        game = Game(player_0, player_1, starts_next_match, reporter)
        #game = Game(player_0, player_1, 0, reporter)
        starts_next_match = game.play()#the loser of the match is returned
        #print(f"====p0:{player_0.score}====p1:{player_1.score}===")
    
    reporter.report_match(player_0.score, player_1.score)
    
    if player_0.score > player_1.score:
        return(0)
    else:
        return(1)
        






    
    

    start = time.time()
    test_matches(n)
    end = time.time()
    total_time = int(end-start)
    print(f"Completed {n} matches in {total_time} seconds.")
   
    
    
    
    