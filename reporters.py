#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Feb 18 13:28:17 2024

@author: zbrogie
"""


#class that updates the users on progress of the game
#will be extended to other classes that have different UIs
class Reporter():
    def __init__(self):
        pass
        
    def report_draw(self, player, decision, shown_card):
        if decision == "deck":
            drawn_card = "a random card"
            drawn_deck = "deck"
        else:
            drawn_card = str(shown_card)
            drawn_deck = "discard pile"
            
        print(f"{player.name} drew {drawn_card} from the {drawn_deck}")
    
    def report_discard(self, player, card):
        print(player.name + " discarded " + str(card))
        
    def report_gin(self, player, losing_score):
        print("Player "+str(player)+" got Gin for "+str(25+losing_score)+" points")
        
    def report_knock(self, player, winning_score, losing_score):
        print("Player "+str(player)+" Knocked with "+str(winning_score)+" points.  Other player had "+str(losing_score))
    
    def report_game_over(self, p0_score, p1_score):
        print(f"Game over: Player 0: {p0_score}, Player1: {p1_score}")
    
    def report_match(self, p0_score, p1_score):
        if p0_score > p1_score:
            print("p0 wins")
        else:
            print("p1 wins")
        print(f"Score  --  p0:{p0_score}, p1:{p1_score}")
        

#only reportsgame over and match over
class Results_Reporter(Reporter):
        
    def report_draw(self, player, decision, shown_card):
        pass
    
    def report_discard(self, player, card):
        pass
        
    def report_gin(self, player, losing_score):
        print("Player "+str(player)+" got Gin for "+str(25+losing_score)+" points")
        
    def report_knock(self, player, winning_score, losing_score):
        print("Player "+str(player)+" Knocked with "+str(winning_score)+" points.  Other player had "+str(losing_score))
    
    def report_game_over(self, p0_score, p1_score):
        print(f"Game over: Player 0: {p0_score}, Player1: {p1_score}")
    
    def report_match(self, p0_score, p1_score):
        if p0_score > p1_score:
            print("p0 wins match")
        else:
            print("p1 wins match")
        print(f"Score  --  p0:{p0_score}, p1:{p1_score}")
        